<?php namespace UMV\Flixproject\FormWidgets;

use Backend\Classes\FormWidgetBase;
use UMV\Flixproject\Models\Attribute;

class AttributeList extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'attributelist';

    public function render() {
        $this->vars['options'] = $this->attributes();

        return $this->makePartial('attributelist');
    }

    public function attributes() {
        $x = Attribute::get();
        $temp = "";
        foreach($x as $y) {
            $temp .= "<option value=\"$y->id\">$y->type - $y->label</option>";
        }
        return $temp;
    }

    public function loadAssets() {
        //$this->addCss('css/select2.css');
        //$this->addJs('js/select2.min.js');
        $this->addJs('js/custom.js');
    }
}
