<?php namespace UMV\Flixproject\Models;

use Model;

/**
 * Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixproject_projects';

    public $belongsTo = [
        'segment' => Segment::class,
        'format' => Format::class,
        'checkout_position' => CheckoutPosition::class,
    ];

    public $belongsToMany = [
        'dependencies' => [Dependency::class, 'table' => 'umv_flixproject_project_dependency']
    ];

    public $attachOne = [
        'file' => ['System\Models\File', 'public' => true]
    ];
}
