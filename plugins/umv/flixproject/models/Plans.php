<?php namespace UMV\Flixproject\Models;

use Model;
use Flash;
use Db;

/**
 * Model
 */
class Plans extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixproject_plans';

    //protected $jsonable = ['attrForm'];
    public $attrForm = null;

    public function afterCreate()
    {
        $_attrForm = json_decode($this->attrForm,true);

        unset($_attrForm['NOME']);
        $pa = array();
        
        foreach($_attrForm as $key => $value) {

            if(is_array($value)) {
                if($value != null && count($value) != 0) {
                    $x = new PlansAttributes();
                    $pa[] = $x->setAttributes_custom((explode("-",$key))[2], implode(";", $value), $this->id);
                }
            } else {
                $value = trim($value);
                if (strpos($key, 'switch-') !== false) {
                    if($value != "0") {
                        $x = new PlansAttributes();
                        $pa[] = $x->setAttributes_custom((explode("-",$key))[2], $value, $this->id);
                    }
                } else if($value != null && $value != "") {
                    $x = new PlansAttributes();
                    $pa[] = $x->setAttributes_custom((explode("-",$key))[2], $value, $this->id);
                }
            }

            
        }

        if(count($pa) == 0) {
            Flash::error('Nâo existem valores a serem armazenados.');
            return false;
        }

        Db::transaction(function () use ($pa) {
            foreach ($pa as $x) {
                $x->save(null, $this->sessionKey);
            }
        });
    }

    public function beforeValidate()
    {
        if($this->attributes == null || count($this->attributes) == 0) {
            Flash::error('Nâo existem valores a serem armazenados.');
            return false;
        }

        if(empty($this->attributes['NOME']) || $this->attributes['NOME'] == "") {
            Flash::error('O NOME deve ser preenchido.');
            return false;
        }

        $this->attrForm = json_encode($this->attributes);

        $name = $this->attributes['NOME'];

        $this->attributes = [];
        $this->attributes['name'] = $name;
    }
}
