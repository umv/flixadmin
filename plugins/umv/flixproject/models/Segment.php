<?php namespace UMV\Flixproject\Models;

use Model;

/**
 * Model
 */
class Segment extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixproject_segments';
}
