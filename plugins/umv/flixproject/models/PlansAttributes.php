<?php namespace UMV\Flixproject\Models;

use Model;

/**
 * Model
 */
class PlansAttributes extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixproject_plans_attributes';

    public function setAttributes_custom($id_umv_flixproject_attributes, $value, $id_umv_flixproject_plans) {
        $this->id_umv_flixproject_attributes = $id_umv_flixproject_attributes;
        $this->value = $value;
        $this->id_umv_flixproject_plans = $id_umv_flixproject_plans;

        return $this;
    }
}
