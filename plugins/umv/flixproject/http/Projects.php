<?php namespace UMV\FlixProject\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixproject\Models\Project;

/**
 * Project Back-end Controller
 */
class Projects extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    public function getProjects(Request $request){

        if(!$request->has('segment'))
            return ['status' => 0, 'mensagem' => 'Segmento da loja vazio ou não encontrado.'];

        $segment = $request->input('segment');

        if($request->has('formats'))
            $formats = $request->input('formats');
        else
            $formats = null;

        if($request->has('size_started_in'))
            $size_started_in = $request->input('size_started_in');
        else
            $size_started_in = null;

        if($request->has('size_finalized_in'))
            $size_finalized_in = $request->input('size_finalized_in');
        else
            $size_finalized_in = null;

        if($request->has('checkout_count_started_in'))
            $checkout_count_started_in = $request->input('checkout_count_started_in');
        else
            $checkout_count_started_in = null;

        if($request->has('checkout_count_finalized_in'))
            $checkout_count_finalized_in = (is_int($request->input('checkout_count_finalized_in')) && $request->input('checkout_count_finalized_in') < 10) ? $request->input('checkout_count_finalized_in') : 999;
        else
            $checkout_count_finalized_in = null;

        if($request->has('checkout_positions'))
            $checkout_positions = $request->input('checkout_positions');
        else
            $checkout_positions = null;

        if($request->has('dependencies'))
            $dependencies = $request->input('dependencies');
        else
            $dependencies = null;

        $result = collect(['status' => 1]);

        try {

            $query = Project::where('umv_flixproject_projects.is_activated', true)
                ->groupBy('umv_flixproject_projects.id');

            if($segment)
                $query = $query->where('umv_flixproject_projects.segment_id', $segment);

            if($size_started_in > 0)
                $query = $query->where('umv_flixproject_projects.size', '>=', $size_started_in);

            if($size_finalized_in > 0)
                $query = $query->where('umv_flixproject_projects.size', '<=', $size_finalized_in);

            if($formats)
                $query = $query->whereIn('umv_flixproject_projects.format_id', $formats);

            if($checkout_count_started_in > 0)
                $query = $query->where('umv_flixproject_projects.checkout_count', '>=', $checkout_count_started_in);

            if($checkout_count_finalized_in > 0)
                $query = $query->where('umv_flixproject_projects.checkout_count', '<=', $checkout_count_finalized_in);

            if($checkout_positions)
                $query = $query->whereIn('umv_flixproject_projects.checkout_position_id', $checkout_positions);

            $projects = $query->get();

            if($projects->count()) {

                foreach ($projects as $project) {
                    $project->segment;
                    $project->format;
                    $project->checkout_position;
                    $project->dependencies;
                    $project->file;
                }

                if($dependencies) {

                    foreach ($dependencies as &$dependency)
                        $dependency = (int) $dependency;

                    $projects = $projects->filter(function ($project, $key) use ($dependencies) {
                        return ($project->dependencies->pluck('id')->intersect($dependencies)->toArray() === $dependencies);
                    })->values();

                }

            }

            if($projects->count()){

                $result = $result->merge([
                    'data' => $projects,
                    'mensagem' => 'Projetos de loja encontrados com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Não encontramos projetos de loja com essas opções. Tente novamente.'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar planta. Tente mais tarde!',
                'exception' => $e->getTraceAsString()
            ]);

            return $result;

        }

    }

    public function getThumbnail(Request $request){

        if(!$request->has('path'))
            return 'https://placehold.it/200';

        $path = $request->input('path');

        $imagick = new \Imagick();
        //$imagick->setResolution( 200, 200 );
        $imagick->readImage( $path );
        $imagick->setbackgroundcolor('rgb(64, 64, 64)');
        $imagick->thumbnailImage(600, 600, true, true);
        $imagick->setImageFormat('jpg');
        header('Content-Type: image/jpeg');
        echo $imagick->getImageBlob();

    }

}
