<?php namespace UMV\FlixProject\Http;

use Backend\Classes\Controller;
use Db;

/**
 * Wizard Back-end Controller
 */
class Wizard extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    public $supported_image = array(
        'gif',
        'jpg',
        'jpeg',
        'png',
        'bmp',
        'tiff',
        'svg'
    );

    public function visualize()
    {
        $_POST = [];
        if(empty($_POST)) {
            $_POST['1-select'] = 'Médio';
            $_POST['2-integer'] = '1';
            $_POST['3-switch'] = '';
            $_POST['5-switch'] = '';
            $_POST['6-checkboxlist'] = [
            0 => 'Retangular',
            1 => 'Quadrada',
            2 => 'Oval',
            3 => 'Triangular'];
            $_POST['7-radio'] = 'Frente';
        }
        
        //$_POST['1-select'] = 'Médio';

        //var_dump($_POST);

        $part1 = "id_umv_flixproject_attributes = ";
        $part2 = " and value";
        $where = [];

        foreach($_POST as $key => $value) {
            $temp = preg_split('/-/',$key);
            $id_attr = $temp[0];
            $type = $temp[1];
            switch($type) {
                case 'text':
                    $where[] = "(".$part1.$id_attr.$part2." like "."'%".$value."%'".")";
                break;
                case 'decimal':
                    $percent = $value * 10 / 100;
                    $where[] = "(".$part1.$id_attr.$part2." between ".($value - $percent)." and ".($value + $percent).")";
                break;
                case 'integer':
                    $percent = $value * 10 / 100;
                    $where[] = "(".$part1.$id_attr.$part2." between ".($value - $percent)." and ".($value + $percent).")";
                break;
                case 'textarea':
                    $where[] = "(".$part1.$id_attr.$part2." like "."'%".$value."%'".")";
                break;
                case 'switch':
                    $where[] = "(".$part1.$id_attr.$part2." = "."1".")";
                break;
                case 'checkboxlist':
                    foreach($value as $_value) {
                        $where[] = "(".$part1.$id_attr.$part2." like "."'%".$_value."%'".")";
                    }
                break;
                case 'radio':
                    if(($value[strlen($value)-1] == '+') && (is_numeric(str_replace("+",$value)))) {
                        $where[] = "(".$part1.$id_attr.$part2." >= "."'".$value."'".")";
                    } else
                        $where[] = "(".$part1.$id_attr.$part2." = "."'".$value."'".")";
                break;
                case 'select':
                    if(($value[strlen($value)-1] == '+') && (is_numeric(str_replace("+",$value)))) {
                        $where[] = "(".$part1.$id_attr.$part2." >= "."'".$value."'".")";
                    } else
                        $where[] = "(".$part1.$id_attr.$part2." = "."'".$value."'".")";
                break;
            }
        }

        //is_numeric
        //if (strpos($a, 'are') !== false) {
        //str_replace

        //var_dump($where);

        $query = DB::table("umv_flixproject_plans_attributes")
        ->groupBy("id_umv_flixproject_plans")
        ->select("id_umv_flixproject_plans as id", DB::raw("COUNT(id_umv_flixproject_plans) AS total"))
        ->orderBy("id","desc")
        ->orderBy("total","desc")
        ->whereRaw(implode(" or ",$where))
        ->get();

        //var_dump($query);

        $ids = array();
        $relevance = array();

        foreach($query as $key => $item) {
            $ids[] = $item->id;
            $relevance[$item->id] = ["relevance" => $item->total];
        }

        //var_dump($ids,$relevance);

        $query = DB::table("umv_flixproject_plans_attributes")
        ->leftJoin("umv_flixproject_attributes","umv_flixproject_plans_attributes.id_umv_flixproject_attributes","umv_flixproject_attributes.id")
        ->leftJoin("umv_flixproject_plans","umv_flixproject_plans_attributes.id_umv_flixproject_plans","umv_flixproject_plans.id")
        ->select("umv_flixproject_plans_attributes.id_umv_flixproject_plans","umv_flixproject_plans_attributes.id_umv_flixproject_attributes",
                "umv_flixproject_plans_attributes.value","umv_flixproject_attributes.type","umv_flixproject_attributes.label",
                "umv_flixproject_plans.name")
        ->whereIn("id_umv_flixproject_plans",$ids)
        ->get();

        foreach($query as $item) {
            $relevance[$item->id_umv_flixproject_plans]['attributes'][] = ['value' => $item->value, 'type' => $item->type, 'label' => $item->label];
            $relevance[$item->id_umv_flixproject_plans]['name'] = $item->name;
        }

        $html = '';

        /*
        <article class="style1">
            <span class="image">
                <img src="images/pic01.jpg" alt="" />
            </span>
            <a href="generic.html">
                <h2>Magna</h2>
                <div class="content">
                    <p>Sed nisl arcu euismod sit amet nisi lorem etiam dolor veroeros et feugiat.</p>
                </div>
            </a>
        </article>
        */
        foreach($relevance as $value) {
            $atts = '';
            $ima = null;
            $url = '#';
            foreach($value['attributes'] as $a) {
                if($a['type'] == 'file') {
                    $url = url('/').'/storage/app/media'.rawurlencode($a['value']);

                    $ext = strtolower(pathinfo($a['value'], PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
                    if (in_array($ext, $this->supported_image)) {
                        $ima = '<span class="image"><img src="'.$url.'" height="350px" width="350px" alt="'.$a['label'].'" /></span>';
                    } else {
                        $ima = '<span class="image"><iframe src="'.$url.'" height="350px" width="350px"></iframe></span>';
                    }
                    //<img src="'.url('/').'/storage/app/media'.$a['value'].'" alt="'.$a['label'].'" />
                } else {
                    if($a['type'] == 'switch') {
                        $atts .= '<span class="title">'.$a['label'].'</span></span>';
                    } else if($a['type'] == 'checkboxlist') {
                        $atts .= '<span class="title">'.$a['label'].' <span class="desc">'.implode(' - ',preg_split('/;/',$a['value'])).'</span></span>';
                    } else
                        $atts .= '<span class="title">'.$a['label'].' <span class="desc">'.$a['value'].'</span></span>';
                    
                    $atts .= '<br>';
                }
            }

            $html .= '<article class="style1">';
            $html .= $ima == null ? '<span class="image"><iframe src="" height="350px" width="350px"></iframe></span>' : $ima;
            $html .= '<a href="'.$url.'">';
            $html .= '<div class="content"><h2>'.$value['name'].'</h2><p>';
            $html .= $atts;
            $html .= '</p></div>';
            $html .= '</a>';
            $html .= '</article>';
        }


        return $html;

    }

    public function formWizard()
    {
        $attrs = DB::table("umv_flixproject_attributes")->orderBy("order","asc")->get();

        $html = '<form id="advanced-form" action="'.url('/').'/api/v1/wizard/visualize" method="POST">';

        foreach($attrs as $item) {
            if($item->type != "file") {
                $html .= '<h3></h3>
                            <fieldset>
                            <h1>'.$item->label.'</h1>
                                <div class="errorTxt"></div><div class="itens">';
                                $html .= $this->buildForm($item);
                                $html .= '</div>';
                if($item->type != "switch") $html .= '<p style="float:left; margin-top:20px; width:100%;">(*) Obrigatório</p>';
                $html .= '</fieldset>';
            }
        }

        $html .= '</form>';

        return $html;
    }

    public function buildForm($att) {
        switch($att->type) {
            case 'label':
                return '<h2 style="color:#ffffff; background-color:#6c757d; padding:5px;">'.$att->value.'</h2>';
            break;
            case 'text':
                return '<input type="text" value="" name="'.$att->id.'-text" class="complete required" style="height:30px;">';
            break;
            case 'decimal':
                return '<input type="number" value="" name="'.$att->id.'-decimal" class="complete inputdecimal required" style="height:30px;">';
            break;
            case 'integer':
                return '<input type="number" value="" name="'.$att->id.'-integer" class="complete inputinteger required" style="height:30px;">';
            break;
            case 'textarea':
                return '<textarea name="'.$att->id.'-textarea" class="complete required"></textarea>';
            break;
            case 'switch':
                $txt = '<div style="float:left;" class="complete customswitch">
                    <label><span style="float:left; font-weight:bold; margin-right:10px; padding-top:8px;">Não</span><label class="switch" style="float:left;">
                        <input type="checkbox" class="success" 
                         style="margin-left:3px;" value="'.$att->value.'" name="'.$att->id.'-switch">
                        <span class="slider round"></span>
                    </label><span style="float:left; font-weight:bold; margin-left:10px; padding-top:8px;">Sim</span></label>
                </div>';
                return $txt;
            break;
            case 'checkboxlist':
                $temp = preg_split('/;/',$att->value);
                $txt = "";
                foreach($temp as $key => $value) {
                    $txt .= '<div class="complete customswitch" style="float:left; margin-bottom:10px;"><span style="margin-left:20px;">'.$value.'</span>
                    <label class="switch" style="float:left;">
                        <input type="checkbox" class="primary required" style="margin-left:3px;" value="'.$value.'" name="'.$att->id.'-checkboxlist[]" >
                        <span class="slider round"></span>                            
                    </label>
                </div>';
                }
                return $txt;
            break;     
            case 'radio':
                $temp = preg_split('/;/',$att->value);
                $txt = "";
                foreach($temp as $key => $value) {
                    $txt .= '<div class="complete customswitch" style="float:left; margin-bottom:10px;"><span style="margin-left:20px;">'.$value.'</span>
                    <label class="switch" style="float:left;">
                        <input type="radio" class="primary required" style="margin-left:3px;" value="'.$value.'" name="'.$att->id.'-radio" >
                        <span class="slider"></span>                            
                    </label>
                </div>';
                }
                //$txt .= '<div style="float:left; width:100%; margin-bottom:10px;"><span style="margin-left:20px;">Nenhum</span>
                //    <label class="switch" style="float:left;">
                //        <input type="radio" class="primary" style="margin-left:3px;" value="" name="'.$att->id.'-radio" checked>
                //        <span class="slider"></span>                            
                //    </label>
                //</div>';
                return $txt;
            break;
            case 'select':
                $temp = array_merge([""],preg_split('/;/',$att->value));
                $txt = '<label for="'.$att->id.'" class="complete"></label>
                        <select name="'.$att->id.'-select" class=" complete required">';
                foreach ($temp as $key => $value) {
                    $txt .= '<option value="'.$value.'">'.$value.'</option>';
                }
                $txt .= '</select>';
                return $txt;
            break;
            default:
                return "INVÁLIDO";
            break;
        }
    }
}
