<?php

Route::group(['prefix' => 'api/v1'], function () {
    //Route::resource('wizard', 'UMV\FlixProject\Http\Wizard');
    Route::get('wizard/teste/{var}', 'UMV\FlixProject\Http\Wizard@teste');
    Route::post('wizard/visualize', 'UMV\FlixProject\Http\Wizard@visualize');
    Route::get('wizard/formwizard', 'UMV\FlixProject\Http\Wizard@formWizard');
});

Route::group(['prefix' => 'api/v1/flixproject'], function () {

    Route::match(['OPTIONS', 'GET'], '/projects', 'UMV\FlixProject\Http\Projects@getProjects');
    Route::match(['OPTIONS', 'GET'], '/projects/thumbnail', 'UMV\FlixProject\Http\Projects@getThumbnail');

});
