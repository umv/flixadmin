<?php namespace UMV\Flixproject;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerFormWidgets()
    {
        return [
            'umv\flixproject\FormWidgets\AttributeList' => 'attributelist'
        ];
    }

    public function registerSettings()
    {
    }
}
