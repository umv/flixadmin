<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectSegments2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_segments', function($table)
        {
            $table->string('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_segments', function($table)
        {
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
