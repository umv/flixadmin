<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectAttributes2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->text('value')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->dropColumn('value');
        });
    }
}