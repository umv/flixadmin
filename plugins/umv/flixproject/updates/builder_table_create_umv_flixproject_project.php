<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixprojectProject extends Migration
{
    public function up()
    {
        Schema::create('umv_flixproject_project', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('segment_id');
            $table->integer('size');
            $table->integer('format_id');
            $table->smallInteger('checkout_count')->nullable();
            $table->smallInteger('checkout_position_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixproject_project');
    }
}
