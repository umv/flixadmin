<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectAttributes4 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->integer('order');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->dropColumn('order');
        });
    }
}