<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectAttributes3 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->dropColumn('relation');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_attributes', function($table)
        {
            $table->text('relation')->nullable();
        });
    }
}