<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixprojectPlans extends Migration
{
    public function up()
    {
        Schema::create('umv_flixproject_plans', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('cod');
            $table->text('value');
            $table->integer('id_umv_flixproject_attributes');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixproject_plans');
    }
}