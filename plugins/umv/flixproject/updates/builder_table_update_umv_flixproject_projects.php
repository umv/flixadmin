<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectProjects extends Migration
{
    public function up()
    {
        Schema::rename('umv_flixproject_project', 'umv_flixproject_projects');
        Schema::table('umv_flixproject_projects', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('umv_flixproject_projects', 'umv_flixproject_project');
        Schema::table('umv_flixproject_project', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
