<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectFormats extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_formats', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_formats', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
