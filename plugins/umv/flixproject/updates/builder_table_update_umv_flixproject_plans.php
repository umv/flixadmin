<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectPlans extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_plans', function($table)
        {
            $table->string('name', 300);
            $table->dropColumn('cod');
            $table->dropColumn('value');
            $table->dropColumn('id_umv_flixproject_attributes');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_plans', function($table)
        {
            $table->dropColumn('name');
            $table->integer('cod');
            $table->text('value');
            $table->integer('id_umv_flixproject_attributes');
        });
    }
}