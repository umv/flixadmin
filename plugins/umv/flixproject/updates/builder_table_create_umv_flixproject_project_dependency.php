<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixprojectProjectDependency extends Migration
{
    public function up()
    {
        Schema::create('umv_flixproject_project_dependency', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('dependency_id');
            $table->integer('project_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixproject_project_dependency');
    }
}
