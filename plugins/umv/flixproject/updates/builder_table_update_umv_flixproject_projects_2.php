<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectProjects2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixproject_projects', function($table)
        {
            $table->boolean('is_activated')->default(1);
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixproject_projects', function($table)
        {
            $table->dropColumn('is_activated');
        });
    }
}
