<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixprojectAttributes extends Migration
{
    public function up()
    {
        Schema::create('umv_flixproject_attributes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('label', 300);
            $table->string('relation')->nullable();
            $table->string('type', 50);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixproject_attributes');
    }
}
