<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixprojectSegments extends Migration
{
    public function up()
    {
        Schema::rename('umv_flixproject_segment', 'umv_flixproject_segments');
        Schema::table('umv_flixproject_segments', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('umv_flixproject_segments', 'umv_flixproject_segment');
        Schema::table('umv_flixproject_segment', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}
