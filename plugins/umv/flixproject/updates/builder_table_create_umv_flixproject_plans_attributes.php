<?php namespace UMV\Flixproject\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixprojectPlansAttributes extends Migration
{
    public function up()
    {
        Schema::create('umv_flixproject_plans_attributes', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_umv_flixproject_attributes');
            $table->integer('id_umv_flixproject_plans');
            $table->text('value');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixproject_plans_attributes');
    }
}