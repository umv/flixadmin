<?php namespace UMV\Flixproject\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use UMV\Flixproject\Models\Plans;
use UMV\Flixproject\Models\Attribute;
use UMV\Flixproject\Models\PlansAttributes;
use Backend\Classes\FormField;

class PlansController extends Controller
{
    /**
     * @var string Default context for "create" pages.
     */
    const CONTEXT_CREATE = 'create';

    /**
     * @var string Default context for "update" pages.
     */
    const CONTEXT_UPDATE = 'update';

    /**
     * @var string Default context for "preview" pages.
     */
    const CONTEXT_PREVIEW = 'preview';

    /**
     * @var \Backend\Classes\Controller|FormController Reference to the back end controller.
     */
    protected $controller;

    /**
     * @var \Backend\Widgets\Form Reference to the widget object.
     */
    protected $formWidget;

    /**
     * @inheritDoc
     */
    protected $requiredProperties = ['formConfig'];

    /**
     * @var array Configuration values that must exist when applying the primary config file.
     * - modelClass: Class name for the model
     * - form: Form field definitions
     */
    protected $requiredConfig = ['modelClass', 'form'];

    /**
     * @var string The context to pass to the form widget.
     */
    protected $context;

    /**
     * @var Model The initialized model used by the form.
     */
    protected $model;





    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixproject', 'main-menu-item', 'side-menu-item2');
    }

    /**
     * Called after the form fields are defined.
     * @param Backend\Widgets\Form $host The hosting form widget
     * @param array $fields Array of all defined form field objects (\Backend\Classes\FormField)
     * @return void
     */
    public function formExtendFields($host, $fields)
    {
        if (!$host->model instanceof Plans) {
            //dd($host);
            return;
        }
        $atts = Attribute::get();
        $customFields = [];
        $customFields = ["NOME" => ["label" => "Nome", "span" => "complete", "type" => "text", "value" => null, "required" => true]];
        
        foreach($atts as $att ) {
            $this->buildForm($att, $customFields);
        }
        //dd($customFields);
        $host->addFields($customFields);
        //dd($host);
    }

    function buildForm($att, &$customFields) {
        switch($att->type) {
            case 'text':
                $customFields["text-".$this->tirarAcentos($att->label)."-".$att->id] = ["label" => $att->label, "span" => "complete", "type" => "text", "value" => $att->value];
            break;
            case 'decimal':
                $customFields["decimal-".$this->tirarAcentos($att->label)."-".$att->id] = ["label" => $att->label, "span" => "complete", "type" => "number", "value" => $att->value, "cssClass" => "inputdecimal"];
            break;
            case 'integer':
                $customFields["integer-".$this->tirarAcentos($att->label)."-".$att->id] = ["label" => $att->label, "span" => "complete", "type" => "number", "value" => $att->value, "cssClass" => "inputinteger"];
            break;
            case 'switch':
                $customFields["switch-".$this->tirarAcentos($att->label)."-".$att->id] = [
                    "label" => $att->label,
                    "span" => "complete",
                    "type" => "switch"];
            break;
            case 'checkboxlist':
                $temp = preg_split('/;/',$att->value);
                $customFields["checkboxlist-".$this->tirarAcentos($att->label)."-".$att->id] = [
                        "label" => $att->label,
                        "span" => "complete",
                        "options" => array_combine($temp, $temp),
                        "type" => "checkboxlist"];
                break;     
            case 'radio':
                $temp = preg_split('/;/',$att->value);
                $customFields["radio-".$this->tirarAcentos($att->label)."-".$att->id] = [
                    "label" => $att->label,
                    "span" => "complete",
                    "options" => array_combine($temp, $temp),
                    "type" => "radio"];
            break;
            case 'select':
                $temp = array_merge([""],preg_split('/;/',$att->value));
                $customFields["select-".$this->tirarAcentos($att->label)."-".$att->id] = [
                    "label" => $att->label,
                    "span" => "complete",
                    "options" => array_combine($temp, $temp),
                    "type" => "dropdown"];
            break; 
            case 'file':
                $customFields["file-".$this->tirarAcentos($att->label)."-".$att->id] = [
                    "label" => $att->label,
                    "mode" => "file",
                    "span" => "complete",
                    "type" => "mediafinder"];
            break;
        }
    }

    function tirarAcentos($str){
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
        return strtolower (preg_replace("/[^a-zA-Z0-9]+/", "", str_replace($a, $b, $str)));
    }
    
}
