<?php namespace UMV\Flixproject\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CheckoutPositions extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'umv_flixproject_checkout_positions' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixproject', 'main-menu-item', 'side-menu-item4');
    }
}
