<?php namespace UMV\Flixproject\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Segments extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'umv_flixproject_segments' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixproject', 'main-menu-item', 'side-menu-item7');
    }
}
