<?php namespace UMV\Flixprice\Classes;

use Closure;

class CorsMiddleware
{

    public function handle($request, Closure $next)
    {

        return $next($request)
            ->header('Access-Control-Allow-Origin', '*');
            //->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, HEAD')
            //->header('Access-Control-Allow-Headers', "Authorization");
    }

}