<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceImports3 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->text('file');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->dropColumn('file');
        });
    }
}
