<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceProducts extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_products', function($table)
        {
            $table->string('ean', 14)->change();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_products', function($table)
        {
            $table->string('ean', 13)->change();
        });
    }
}
