<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceImports extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->string('file_id');
            $table->increments('id')->unsigned(false)->change();
            $table->string('status')->change();
            $table->dropColumn('file');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->dropColumn('file_id');
            $table->increments('id')->unsigned()->change();
            $table->string('status', 191)->change();
            $table->string('file', 191);
        });
    }
}