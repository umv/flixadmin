<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceImports5 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->string('file')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('percentage')->nullable(false)->unsigned(false)->default(null)->change();
            $table->string('message')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->text('file')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('percentage')->nullable(false)->unsigned(false)->default(null)->change();
            $table->text('message')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
