<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceImports4 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->text('percentage');
            $table->text('message');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->dropColumn('percentage');
            $table->dropColumn('message');
        });
    }
}
