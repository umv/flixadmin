<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixpriceImports2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->dropColumn('archive');
            $table->dropColumn('file');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixprice_imports', function($table)
        {
            $table->string('archive', 191);
            $table->string('file', 191);
        });
    }
}
