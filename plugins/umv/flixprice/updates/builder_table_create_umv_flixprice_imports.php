<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixpriceImports extends Migration
{
    public function up()
    {
        Schema::create('umv_flixprice_imports', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('file');
            $table->string('status')->default('Aguardando');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixprice_imports');
    }
}