<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration104 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixprice_prices', function ($table) {        
            $table->foreign('product_id')->references('id')->on('umv_flixprice_products');
            $table->foreign('state_id')->references('id')->on('wingmidia_localization_states');
        });
    }

    public function down()
    {
        Schema::table('umv_flixprice_prices', function ($table) {        
            $table->dropForeign('umv_flixprice_prices_product_id_foreign');
            $table->dropForeign('umv_flixprice_prices_state_id_foreign');
        });
    }
}