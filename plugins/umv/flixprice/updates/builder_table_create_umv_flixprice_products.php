<?php namespace UMV\Flixprice\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixpriceProducts extends Migration
{
    public function up()
    {
        Schema::create('umv_flixprice_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('ean', 13);
            $table->string('name', 255)->nullable();
            $table->boolean('is_activated')->default(1);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixprice_products');
    }
}
