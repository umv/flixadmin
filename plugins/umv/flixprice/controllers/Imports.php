<?php namespace UMV\Flixprice\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Imports extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'umv_flixprice_imports' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixprice', 'main-menu-item', 'side-menu-item2');
    }
    
}
