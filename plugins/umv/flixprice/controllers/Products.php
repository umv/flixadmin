<?php namespace UMV\Flixprice\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Products extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'umv_flixprice_products' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixprice', 'main-menu-item', 'side-menu-item');
    }
}
