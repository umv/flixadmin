<?php namespace UMV\Flixprice\Models;

use Model;
use October\Rain\Exception\ApplicationException;

/**
 * Model
 */
class Import extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixprice_imports';

    public $attachOne = [
        'file' => ['System\Models\File', 'public' => false]
    ];

    public function getFileNameAttribute()
    {
        return $this->file()->first()->file_name;
    }

    /*public function isWaiting(){

        return ($this->status === 'Em andamento') ? true : false;

    }

    public function beforeSave(){

        if (!$this->isWaiting())
            throw new ApplicationException('⛔ Esta importação já esta em andamento!');

    }*/
}
