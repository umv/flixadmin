<?php namespace UMV\Flixprice\Models;

use Model;

/**
 * Model
 */
class Product extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixprice_products';

    public $hasMany = [
        'prices' => Price::class,
    ];

    protected $guarded = [];
}
