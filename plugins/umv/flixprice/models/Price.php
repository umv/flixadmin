<?php namespace UMV\Flixprice\Models;

use Model;
use WingMidia\Localization\Models\State;

/**
 * Model
 */
class Price extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixprice_prices';

    protected $dates = ['date'];

    protected $guarded = [];

    public $belongsTo = [
        'product' => Product::class,
        'state' => State::class
    ];

    /*public function setPriceAttribute($value){

        $this->attributes['price'] = number_format(str_replace(',', '.', str_replace('.', '', $value)),2,'.','');

    }

    public function getPriceAttribute($value){

        return number_format($value, 2, ',', '.');

    }*/
}
