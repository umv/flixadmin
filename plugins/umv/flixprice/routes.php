<?php

Route::group(['prefix' => 'api/v1/flixprice'], function () {

    Route::match(['OPTIONS', 'GET'], '/price', 'UMV\Flixprice\Http\Price@getPriceByEstate');
    
});
