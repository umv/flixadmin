<?php namespace UMV\Flixprice;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public $require = ['WingMidia.Localization'];

    public function register()
    {
        $this->registerConsoleCommand('flixprice:importfile', 'UMV\Flixprice\Console\ImportFile');
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerListColumnTypes()
    {
        return [
            'money' => [$this, 'evalMoneyColumn']
        ];
    }

    public function evalMoneyColumn($value, $column, $record)
    {
        return 'R$ ' . number_format($value, 2, ',', '.');
    }
}
