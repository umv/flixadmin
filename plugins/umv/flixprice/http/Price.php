<?php namespace UMV\Flixprice\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixprice\Models\Price as PriceModel;

/**
 * Price Back-end Controller
 */
class Price extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    public function getPriceByEstate(Request $request){

        if(!$request->has('ean'))
            return ['status' => 0, 'mensagem' => 'EAN do produto vazio ou não encontrado.'];

        if(!$request->has('uf'))
            return ['status' => 0, 'mensagem' => 'UF do estado vazio ou não encontrado.'];

        $ean = $request->input('ean');
        $uf = $request->input('uf');

        $result = collect(['status' => 1]);

        try{

            $price = PriceModel::select(
                    'umv_flixprice_products.ean',
                    'umv_flixprice_products.name AS product',
                    'wingmidia_localization_states.name AS state',
                    'wingmidia_localization_states.uf',
                    'umv_flixprice_prices.date',
                    'umv_flixprice_prices.price'
                )
                ->join('umv_flixprice_products', 'umv_flixprice_products.id', '=', 'umv_flixprice_prices.product_id')
                ->join('wingmidia_localization_states', 'wingmidia_localization_states.id', '=', 'umv_flixprice_prices.state_id')
                ->where([
                    ['umv_flixprice_products.is_activated', true],
                    ['umv_flixprice_products.ean', $ean],
                    ['wingmidia_localization_states.uf', $uf]
                ])
                ->first();

            $others = PriceModel::select(
                'umv_flixprice_products.ean',
                'umv_flixprice_products.name AS product',
                'wingmidia_localization_states.name AS state',
                'wingmidia_localization_states.uf',
                'umv_flixprice_prices.date',
                'umv_flixprice_prices.price'
            )
                ->join('umv_flixprice_products', 'umv_flixprice_products.id', '=', 'umv_flixprice_prices.product_id')
                ->join('wingmidia_localization_states', 'wingmidia_localization_states.id', '=', 'umv_flixprice_prices.state_id')
                ->where([
                    ['umv_flixprice_products.is_activated', true],
                    ['umv_flixprice_products.ean', $ean],
                    ['wingmidia_localization_states.uf', '<>', $uf]
                ])
                ->get();

            if($price){

                $result = $result->merge([
                    'data' => $price,
                    'others' => $others,
                    'mensagem' => 'Preço encontrado com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'others' => $others,
                    'mensagem' => 'Preço não encontrado!'
                ]);

            }
            
            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar preço!',
                'exception' => $e->getMessage()
            ]);

            return $result;

        }

    }

}
