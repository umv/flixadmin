<?php namespace UMV\Flixprice\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use UMV\Flixprice\Models\Import;
use UMV\Flixprice\Models\Price;
use UMV\Flixprice\Models\Product;
use Vdomah\Excel\Classes\Excel;
use WingMidia\Localization\Models\State;

class ImportFile extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'flixprice:importfile';

    /**
     * @var string The console command description.
     */
    protected $description = 'Processar o arquivo para importação';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        ini_set( 'display_errors', 'on' );
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        ini_set('max_input_time', 300);

        $this->output->writeln('== Iniciando importações == ');

        $import = Import::where('status', 'Aguardando')
            ->first();

        if($import) {

            $this->output->writeln(" == Importando planilha: {$import->id}");

            $import->status = 'Em andamento';

            $import->save();

            $this->import($import);

            $this->output->writeln('== Planilha concluída =D');

        } else {

            $this->output->writeln('== Nenhuma planilha encontrada =D');

        }

        $this->output->writeln('== Importações finalizadas == ');
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

    protected function import($import){

        if($import->file){

            try{

                $file_path = $import->file->getLocalPath();

                Excel::excel()->load($file_path, function($reader) use ($import) {

                    $states = State::pluck('id', 'uf');
                    $date = Carbon::now();

                    $results = $reader->get();

                    $bar = $this->output->createProgressBar(count($results));

                    foreach ($results as $data){

                        if(!empty($data['ean'])) {

                            $product = Product::updateOrCreate(
                                [
                                    'ean' => $data['ean'],
                                ],
                                [
                                    'name' => $data['produto'],
                                ]
                            );

                            if($product) {

                                Price::updateOrCreate(
                                    [
                                        'product_id' => $product->id,
                                        'state_id' => $states[$data->uf],
                                    ],
                                    [
                                        'price' => $data['preco'],
                                        'date' => $date,
                                    ]
                                );

                            }

                        }

                        // barra de progresso

                        $percentage = (int)($bar->getProgressPercent() * 100);

                        if($import && ($percentage%10)===0){
                            $import->percentage = $percentage;
                            $import->save();
                        }

                        $bar->advance();

                    }

                    $bar->finish();

                }, null, true);

                $import->status = 'Completo';
                $import->percentage = '100%';
                $import->update();

            } catch (\Exception $e){

                $import->status = 'Erro';
                $import->message = $e->getMessage();// . ' - ' . $e->getTraceAsString();
                $import->update();

            }

        }

    }
}
