<?php

Route::group(['prefix' => 'api/auth'], function () {

    Route::options('login', function (Request $request) {
        return response()->json();
    });

    Route::options('me', function (Request $request) {
        return response()->json();
    });

});

Route::group(['prefix' => 'api/v1/flix'], function () {

    Route::match(['OPTIONS', 'POST'], '/auth', 'UMV\Flix\Http\Auth@login');

});