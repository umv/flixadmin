<?php namespace UMV\Flix\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use RainLab\User\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class Auth extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    protected $url_validation = "https://www.flixdovarejo.com.br/validar-o-token-de-parceria";
    protected $url_decryption = "https://www.flixdovarejo.com.br/validar-o-acesso-do-assinante";

    protected $key_partner = "9FEF2C66A9E91F936C7BBCFCC76BEF44";

    public function login(Request $request){

        if(!$request->has('kc'))
            return ['status' => 0, 'mensagem' => 'Chave do cliente vazia ou não encontrado.'];

        if(!$request->has('ka'))
            return ['status' => 0, 'mensagem' => 'Chave de autorização vazia ou não encontrado.'];

        $key_client = $request->input('kc');
        $key_autorization = $request->input('ka');

        $result = collect(['status' => 1]);

        try{

            $xmlstr =  $this->validation($key_client, $key_autorization);

            libxml_use_internal_errors(true);

            $xml_encrypted = simplexml_load_string($xmlstr);

            if (!$xml_encrypted) {

                $xmlstr = $this->descryption($xmlstr);

                $xml_descrypted = simplexml_load_string($xmlstr);

                if ($xml_descrypted && !empty($xml_descrypted->cpf)) {

                    $user = \RLuders\JWTAuth\Models\User::where('cpf', $xml_descrypted->cpf)
                        ->first();

                    $data = [
                        'cpf' => $xml_descrypted->cpf,
                        'cnpj' => $xml_descrypted->cnpj,
                        'assinatura' => $xml_descrypted->assinatura,
                        'surname' => $xml_descrypted->razaoSocial,
                        'name' => $xml_descrypted->nome,
                        'email' => $xml_descrypted->email
                    ];

                    $password = md5($xml_descrypted->senha);

                    if(!$user){

                        $register_data = [
                            'name' => $xml_descrypted->nome,
                            'email' => $xml_descrypted->email,
                            'password' => $password,
                            'password_confirmation' => $password,
                        ];

                        $user = \RainLab\User\Facades\Auth::register($register_data, true);
                        $user = \RLuders\JWTAuth\Models\User::where('cpf', $user->cpf)
                            ->first();

                    }

                    $user->fill($data)->save();

                    $token = JWTAuth::fromUser($user);

                    return response()->json(compact('token', 'user'));

                    /*$result = $result->merge([
                        //'xml_descrypted' => $xml_descrypted,
                        'token' => $token,
                        'user' => $user->toArray(),
                        //'mensagem' => 'Preço encontrado com sucesso.'
                    ]);*/

                } else {

                    // RESPONSE DESCRYPTION INVALID

                    return redirect("https://www.flixdovarejo.com.br/complete-os-dados-do-administrador?KC={$key_client}&KA={$key_autorization}&KU=".base64_encode('http://flixmidia.maxmix.net.br/auth/flix/')."&PA=Flixmidia");

                }

            } else {



            }
            
            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao autenticar usuário!',
                'exception' => $e->getMessage()
            ]);

            return $result;

        }

    }

    protected function validation($key_client, $key_autorization){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_validation,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "KC=".$key_client."&KA=".$key_autorization."&KP=".$this->key_partner,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error";
        } else {
            return $response;
        }

    }

    protected function descryption($code){

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url_decryption,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "COD=".$code."&KP=".$this->key_partner,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error";
        } else {
            return $response;
        }

    }

}
