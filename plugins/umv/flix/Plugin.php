<?php namespace UMV\Flix;

use Yaml;
use File;
use System\Classes\PluginBase;
use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Classes\UserEventBase;

class Plugin extends PluginBase
{

    public $require = ['RainLab.User'];

    public function boot()
    {

        $this->extendUserModel();
        $this->extendUsersController();
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    protected function extendUserModel()
    {
        UserModel::extend(function($model) {
            $model->addFillable([
                'cpf',
                'cnpj',
                'assinatura'
            ]);
        });
    }

    protected function extendUsersController()
    {
        UsersController::extendFormFields(function($widget) {
            // Prevent extending of related form instead of the intended User form
            if (!$widget->model instanceof UserModel) {
                return;
            }
            $configFile = plugins_path('umv/flix/config/user_fields.yaml');
            $config = Yaml::parse(File::get($configFile));
            $widget->addTabFields($config);
        });
    }

}
