<?php namespace RainLab\UserPlus\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
class UserAddProfileFields extends Migration
{
    public function up()
    {
        if (Schema::hasColumns('users', ['cpf', 'cnpj'])) {
            return;
        }
        Schema::table('users', function($table)
        {
            $table->string('cpf', 11)->nullable();
            $table->string('cnpj', 14)->nullable();
        });
    }
    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['cpf', 'cnpj']);
            });
        }
    }
}