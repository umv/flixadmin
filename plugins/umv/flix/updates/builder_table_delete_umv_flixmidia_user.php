<?php namespace UMV\Flix\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteUmvFlixmidiaUser extends Migration
{
    public function up()
    {
        Schema::dropIfExists('umv_flixmidia_user');
    }
    
    public function down()
    {
        Schema::create('umv_flixmidia_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('name');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
}
