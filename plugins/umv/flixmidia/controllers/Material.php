<?php namespace UMV\Flixmidia\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Material extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('UMV.Flixmidia', 'main-menu-item', 'Material');
    }
    public function blank(){}
}