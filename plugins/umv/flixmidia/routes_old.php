<?php

Route::group(['prefix' => 'api/v1/flixmidia'], function () {
    //Route::resource('material', 'UMV\Flixmidia\Http\MaterialRest');
    //Get all materials
    Route::match(['OPTIONS', 'GET'], '/material', 'UMV\Flixmidia\Http\MaterialRest@listAllMaterials');
    //Get single material by id
    Route::match(['OPTIONS', 'GET'],'/material/{id}', 'UMV\Flixmidia\Http\MaterialRest@getMaterialById');
    //Get material by user id
    Route::match(['OPTIONS', 'GET'],'/material/user/{userId}', 'UMV\Flixmidia\Http\MaterialRest@getAllMaterialByUser');
    //Get all page
    Route::match(['OPTIONS', 'GET'],'/page/{idMaterial}', 'UMV\Flixmidia\Http\PageRest@listAllPagesByMaterial');
    //Get all page objects
    Route::match(['OPTIONS', 'GET'],'/pageobject', 'UMV\Flixmidia\Http\PageRest@getAllPageObjects');
    //Get all page object by pageID and ObjectTypeID
    Route::match(['OPTIONS', 'GET'],'/pageobject/{pageId}/{objectTypeId}', 'UMV\Flixmidia\Http\PageRest@getPageObjectByPageAndObjectTypeId');
    //Get page by Tema Vs PaginaID vs Tipo.
    Route::match(['OPTIONS', 'GET'],'/page/{idTema}/{idPagina}/{idTipo}', 'UMV\Flixmidia\Http\PageRest@getPageObjectByArgs');
    // Get all temapage
    Route::match(['OPTIONS', 'GET'],'/themepage', 'UMV\Flixmidia\Http\ThemeRest@getAllTheme');
    // Get all tema by page
    Route::match(['OPTIONS', 'GET'],'/theme/', 'UMV\Flixmidia\Http\ThemeRest@getAllThemeByPage');

});