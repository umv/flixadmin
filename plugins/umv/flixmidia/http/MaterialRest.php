<?php
/**
 * Created to Wingmidia.
 * User: puchalski
 * Date: 05/10/18
 * Time: 17:20
 */
namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Doctrine\DBAL\Query\QueryException;
use UMV\Flixmidia\Models\Material;
use UMV\Flixmidia\Models\MaterialUser;

class MaterialRest extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';
    private $DBPREFIX = 'umv_flixmidia_';

    public function __construct()
    {
        parent::__construct();
        //BackendMenu::setContext('UMV.Flixmidia', 'main-menu-item', 'Material');

    }
    public function  blank(){

    }

    /**
     * Check if variable is integer.
     * @param $val Variable to check value
     * @return false|integer if variable is not integer return a die, if is return itself.
     */
    private function checkInteger($val){
        if(!assert(is_integer($val), 'ID must be a integer')){
            return die;
        }
        else{
            return $val;
        }
    }


    /**
     * @brief Get all materials by Tema
     * @param $tema Tema id from databank
     * @return Array with Bank.
     */
    public function getMaterialById($id){
        $value = $this->checkInteger($id);
        return Material::find($value);
    }

    /**
     * Return all list from material table.
     * @return false|string Return the Json Result
     */
    public function listAllMaterials(){
        try{
            return Material::where('umv_flixmidia_material.is_activated', '=', '1')->get();
        } catch(QueryException $qex){
            printf("An error has ocurred!\nError Info: %s", $qex->getMessage());
            $val = [
                "error_code" => $qex->getCode(),
                "message_error" => $qex->getMessage()
            ];
            return json_encode($val);
        }

    }

    /**
     * @param $id the id for using in query.
     * @return false|string return all material by user.
     */
    public function getAllMaterialByUser($id){
        $value = $this->checkInteger($id);
        try{
            MaterialUser::join(
                $this->DBPREFIX.'material',
                $this->DBPREFIX.'material_user.material_id',
                '=',
                $this->DBPREFIX.'material.id')
                ->where($this->DBPREFIX.'material_user.user_id', '=', $id)
                ->get();
        } catch (QueryException $qex){
            printf("An error has ocurred!\nError Info: %s", $qex->getMessage());
            $val = [
                "error_code" => $qex->getCode(),
                "message_error" => $qex->getMessage()
            ];
            return json_encode($val);
        }


    }



}