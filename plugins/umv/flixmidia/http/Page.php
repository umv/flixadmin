<?php namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixmidia\Models\Page as PageModel;
use UMV\Flixmidia\Models\PageObject;
use UMV\Flixmidia\Models\PageObjectType;

/**
 * Page Back-end Controller
 */
class Page extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    protected function getPageObjects($page_id){

        $objects = PageObject::where('page_id', $page_id)
            ->get();

        foreach ($objects as &$object) {

            unset($object->created_at);
            unset($object->updated_at);

            $type = PageObjectType::find($object->page_object_type_id);

            unset($type->created_at);
            unset($type->updated_at);

            $object->type = $type;

        }

        return $objects;

    }

    public function getAll(Request $request){

        if(!$request->has('material_id'))
            return ['status' => 0, 'mensagem' => 'Código do material vazio ou não encontrado.'];

        $material_id = $request->input('material_id');

        $result = collect(['status' => 1]);

        try{

            $pages = PageModel::where('material_id', $material_id)
                ->get();

            if($pages->count() > 0){

                foreach ($pages as &$page) {

                    unset($page->created_at);
                    unset($page->updated_at);

                    $page->objects = $this->getPageObjects($page->id);
                }

                $result = $result->merge([
                    'data' => $pages,
                    'mensagem' => 'Páginas encontradas com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Nenhuma página encontrado!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar páginas!',
                'exception' => $e->getMessage()
            ]);

            return $result;

        }

    }

    public function getPage(Request $request){

        if(!$request->has('page_id'))
            return ['status' => 0, 'mensagem' => 'Código da página vazio ou não encontrado.'];

        $page_id = $request->input('page_id');

        $result = collect(['status' => 1]);

        try{

            $page = PageModel::find($page_id);

            if($page){

                unset($page->created_at);
                unset($page->updated_at);

                $page->objects = $this->getPageObjects($page->id);

                $result = $result->merge([
                    'data' => $page,
                    'mensagem' => 'Página encontrada com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Página não encontrada!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar página!',
                'exception' => $e->getMessage()
            ]);

            return $result;

        }

    }


}
