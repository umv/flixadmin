<?php namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use UMV\Flixmidia\Models\Theme as ThemeModel;
use UMV\Flixmidia\Models\ThemePage;
use UMV\Flixmidia\Models\ThemePrice;

/**
 * Themes Back-end Controller
 */
class Themes extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    protected function formatFields($item, $relation, $field){

        $path = '';

        if (isset($item->{$relation}))
            $path = $item->{$relation}->getPath();

        unset($item->{$relation});
        unset($item->created_at);
        unset($item->updated_at);

        $item->{$field} = $path;

        return $item;

    }

    protected function getThemePages($theme_id, $material_id){

        $pages = ThemePage::select('umv_flixmidia_theme_page.*', 'umv_flixmidia_page.name')
            ->join('umv_flixmidia_page', 'umv_flixmidia_page.id', '=', 'umv_flixmidia_theme_page.page_id')
            ->where([
                ['umv_flixmidia_theme_page.theme_id', $theme_id],
                ['umv_flixmidia_page.material_id', $material_id]
            ])
            ->get();

        foreach ($pages as &$page)
            $page = $this->formatFields($page, 'background', 'background_path');

        return $pages;

    }

    protected function getThemePrices($theme_id){

        $prices = ThemePrice::where('theme_id', $theme_id)
            ->get();

        foreach ($prices as &$price)
            $price = $this->formatFields($price, 'background', 'background_path');

        return $prices;

    }

    public function getAll(Request $request){

        if(!$request->has('material_id'))
            return ['status' => 0, 'mensagem' => 'Código do material vazio ou não encontrado.'];

        $material_id = $request->input('material_id');

        $result = collect(['status' => 1]);

        try{

            $themes = ThemeModel::select('umv_flixmidia_theme.*')
                ->join('umv_flixmidia_theme_page', 'umv_flixmidia_theme_page.theme_id', '=', 'umv_flixmidia_theme.id')
                ->join('umv_flixmidia_page', 'umv_flixmidia_page.id', '=', 'umv_flixmidia_theme_page.page_id')
                ->where('umv_flixmidia_page.material_id', $material_id)
                ->whereRaw("DATE(umv_flixmidia_theme.started_at) <= '".Carbon::today()->toDateString()."' AND DATE(umv_flixmidia_theme.finalized_at) >= '".Carbon::today()->toDateString()."'")
                ->groupBy('umv_flixmidia_theme.id')
                ->get();

            if($themes->count() > 0){

                foreach ($themes as &$theme) {

                    $theme = $this->formatFields($theme, 'cover', 'cover_path');

                    $theme->pages = $this->getThemePages($theme->id, $material_id);
                    $theme->prices = $this->getThemePrices($theme->id);
                }

                $result = $result->merge([
                    'data' => $themes,
                    'mensagem' => 'Temas encontrados com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Nenhum tema encontrado!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar temas!',
                'xception' => $e->getMessage()
            ]);

            return $result;

        }

    }

    public function getThemeByMaterial(Request $request){

        if(!$request->has('material_id'))
            return ['status' => 0, 'mensagem' => 'Código do material vazio ou não encontrado.'];

        if(!$request->has('theme_id'))
            return ['status' => 0, 'mensagem' => 'Código do tema vazio ou não encontrado.'];

        $material_id = $request->input('material_id');
        $theme_id = $request->input('theme_id');

        $result = collect(['status' => 1]);

        try{

            $theme = ThemeModel::select('umv_flixmidia_theme.*')
                ->join('umv_flixmidia_theme_page', 'umv_flixmidia_theme_page.theme_id', '=', 'umv_flixmidia_theme.id')
                ->join('umv_flixmidia_page', 'umv_flixmidia_page.id', '=', 'umv_flixmidia_theme_page.page_id')
                ->where([
                    ['umv_flixmidia_page.material_id', $material_id],
                    ['umv_flixmidia_theme.id', $theme_id]
                ])
                ->whereRaw("DATE(umv_flixmidia_theme.started_at) <= '".Carbon::today()->toDateString()."' AND DATE(umv_flixmidia_theme.finalized_at) >= '".Carbon::today()->toDateString()."'")
                ->groupBy('umv_flixmidia_theme.id')
                ->first();

            if($theme){

                $theme = $this->formatFields($theme, 'cover', 'cover_path');

                $theme->pages = $this->getThemePages($theme->id, $material_id);
                $theme->prices = $this->getThemePrices($theme->id);

                $result = $result->merge([
                    'data' => $theme,
                    'mensagem' => 'Tema encontrado com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Teme não encontrado!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar tema!'
            ]);

            return $result;

        }

    }

}
