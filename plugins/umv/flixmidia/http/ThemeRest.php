<?php
/**
 * Created by PhpStorm.
 * User: puchalski
 * Date: 09/10/18
 * Time: 16:39
 */

namespace UMV\Flixmidia\Http;


use Carbon\Carbon;
use Cms\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixmidia\Models\Page;
use UMV\Flixmidia\Models\Theme;
use UMV\Flixmidia\Models\ThemePage;
use UMV\Flixmidia\Models\ThemePrice;

class ThemeRest extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];


    public function __construct()
    {
        parent::__construct();
    }

    public function blank(){

    }

    public function getAllTheme()
    {
        $date = Carbon::now();
        //Get all list for what we need!
        $root = Theme::whereDate('finalized_at', '>', $date)->get();
        //Get paths by iterate cover if is not null
        foreach ($root as $value){
            if(!empty($value->cover)){
                $value->cover;
            }
        }
        return $root;
    }


    /**
     * * Result from query for all ThemePage.
     * @param Request $request Request param from url.
     * @return mixed get json resul wit img_background.
     */
    public function getAllThemeByPage(){
        // Get all cruds
        try{
            $date = Carbon::now();
            //Get all list for what we need!
            $root = Theme::whereDate('finalized_at', '>', $date)
                ->where('id', '=', $_GET['idtema'])
                ->get();

            $themePage = ThemePage::join('umv_flixmidia_theme', 'umv_flixmidia_theme_page.theme_id' , 'umv_flixmidia_theme.id')
                ->join('umv_flixmidia_page', 'umv_flixmidia_theme_page.page_id' , 'umv_flixmidia_page.id')
                ->where('umv_flixmidia_theme_page.theme_id', '=', $_GET['idtema'])
                ->get();
            //dd($themePage);
            // Foreach Get images path, before join
            $arrAux = [];



            foreach (ThemePage::get() as $p){


                foreach ($themePage as $t){
                  // dd ($t->attributes);

                    if($t->attributes["theme_id"] == $p->theme_id && $t->attributes["page_id"] == $p->page_id ){

                        //$t->attributes["name"] =  $p->coverPage->path;
                        $arrAux[] = [$t, $p->coverPage->path];
                    }
                }
            };
            //dd($arrAux);


            $themePageObject = ThemePage::where('theme_id','=',$_GET['idtema'])
                ->join('umv_flixmidia_page_object', 'umv_flixmidia_theme_page.page_id', 'umv_flixmidia_page_object.id')
                ->get();

            $themePrice = ThemePrice::where('theme_id','=',$_GET['idtema'])
                ->get();

            // if we have fail to get cruds, throw exception
            if(is_null($themePage) && is_null($themePrice) && is_null($themePrice)){
                throw new ModelException('We can\'t get model values from DB');
            }

        } catch (ModelException $mex) {
            printf("An error has ocurred!\nError Info: %s", $mex->getMessage());
            $val = [
                "error_code" => $mex->getCode(),
                "message_error" => $mex->getMessage()
            ];
            return json_encode($val);
        }



        //Get paths by iterate cover if is not null
        foreach ($root as $value) {
            //Set the values
            //$value->themepages = $themePage;
            $value->themepages = $arrAux;
            $value->themeprice = $themePrice;

            /*
            foreach ($value->themepages as $it) {
                dd($it->coverPage->path);
            }*/
            // Foreach Theme pages
            foreach ($value->themepages as $item) {

                $item[0]->coverPage = $item[1];
                /*if (!empty($item[0]->coverPage)) {
                    $item[0]->coverPage = $item[1];
                }*/
                $item[0]->objpages = $themePageObject;

            }
            foreach ($value->themeprice as $_themePrice){
                if(!empty($_themePrice->coverPrice)) {
                    $_themePrice->coverPrice;
                }
            }

            if (!empty($value->cover)) {
                $value->cover;
            }
        }
        
        return $root;
    }
}