<?php namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;

/**
 * Storage Back-end Controller
 */
class Storage extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    protected $url_storage = 'https://storage.flixmidia.maxmix.net.br';

    protected $max_filesize = 10000000; // em bytes

    protected $extensions = ['png', 'jpg', 'jpeg', 'pdf'];

    protected function url($path){

        return $this->url_storage . '/' . $path;

    }

    public function store(Request $request){

        $file = $request->file('object');

        if(!$request->hasFile('object') || !$file->isValid())
            return ['status' => 0, 'mensagem' => 'Arquivo inválido, vazio ou não encontrado.'];

        if($file->getClientSize() > $this->max_filesize)
            return ['status' => 0, 'mensagem' => 'O arquivo enviado é maior que o permitido: 2 Megabyte.'];

        if(!in_array(strtolower($file->guessExtension()), $this->extensions))
            return ['status' => 0, 'mensagem' => 'Tipo de arquivo não aceito.'];

        $result = collect(['status' => 1]);

        try{

            $name = str_random(20).'.'.$file->guessExtension();

            $path = $request->file('object')->storeAs('uploads', $name, 'ftp_flixmidia');

            $result = $result->merge([
                'path' => $this->url($path),
                'mensagem' => 'Arquivo salvo com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e){

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao salvar arquivo! ' . $e->getMessage()
            ]);

            return $result;

        }

    }

}
