<?php
/**
 * Created to Wingmidia.
 * User: puchalski
 * Date: 05/10/18
 * Time: 17:20
 */

namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Config;
use October\Rain\Database\ModelException;
use October\Rain\Exception\ExceptionBase;
use UMV\Flixmidia\Models\MaterialUser;
use UMV\Flixmidia\Models\Page;
use UMV\Flixmidia\Models\PageObject;


class PageRest extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    public function __construct()
    {
        parent::__construct();
    }

    public function blank()
    {

    }

    /**
     * Check if variable is integer.
     * TO-DO: Need check why is throwing exception every time.
     * @param $val Variable to check value
     * @return false|integer if variable is not integer return a die, if is return itself.
     */
    private function checkInteger($val)
    {
        try {
            if (!is_integer($val)) {
                throw new ExceptionBase('ID must be a integer');
                return die;
            }
            return $val;
        } catch (ExceptionBase $eb) {
            printf("An error has occurred!\nError Info: %s", $eb->getMessage());
            $vex = array(
                "error_code" => $eb->getCode(),
                "message_error" => $eb->getMessage()
            );

            return json_encode($vex);
        }
        return $val;
    }

    /**
     * @brief Get all materials by Tema
     * @param $id ID from databank
     * @return Array with result from query.
     */
    public function getPageById($id)
    {
        assert(is_integer($id), 'ID must be a integer');
        return Page::find($id);
    }

    /**
     * Return all list from material table.
     * @return false|string Return the Json Result
     */
    public function listAllPagesByMaterial($materialId)
    {

        $val = Page::where('umv_flixmidia_page.material_id', '=', $materialId)->get();
        return $val;
    }

    /**
     * Get all Page objects
     * @return mixed return the JSON with informations
     */
    public function getAllPageObjects()
    {
        try {
            $val = PageObject::all();
            if (is_nul($val))
                throw ModelException('Error in executiong PageObject::all()');
        } catch (ModelException $mex) {
            printf("An error has ocurred!\nError Info: %s", $mex->getMessage());
            $val = [
                "error_code" => $mex->getCode(),
                "message_error" => $mex->getMessage()
            ];
            return json_encode($val);
        }

    }

    /**
     * Get all page object by Page and Object ID
     * @param $pageId The page id for query
     * @param $pageObjectTypeId The pageObjectType id for query.
     * @return false|string if query sucessfull we will return the result.
     * if we got exception we go return json with error.
     */
    public function getPageObjectByPageAndObjectTypeId($pageId, $pageObjectTypeId)
    {
        assert(is_integer($pageId), 'ID must be a integer');
        assert(is_integer($pageObjectTypeId), 'ID must be a integer');
        try {
            $val = PageObject::where('umv_flixmidia_page_object.page_id', '=', $pageId, 'AND', 'umv_flixmidia_page_object.page_object_type_id', '=', $pageObjectTypeId)->get();
            if (is_null($val))
                throw new ModelException('Error!');

        } catch (ModelException $mex) {
            printf("An error has ocurred!\nError Info: %s", $mex->getMessage());
            $val = [
                "error_code" => $mex->getCode(),
                "message_error" => $mex->getMessage()
            ];
            return json_encode($val);
        }
    }

    /**
     * Save Material User info with json!
     * @param $json_in
     */
    public function uploadDataToMaterialUser($json_in)
    {
        // Array in
        MaterialUser::save($json_in);
    }
}