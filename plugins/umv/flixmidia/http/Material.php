<?php namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixmidia\Models\Material as MaterialModel;

/**
 * Material Back-end Controller
 */
class Material extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    protected function formatFields($material){

        $icon_path = '';

        if (isset($material->icon))
            $icon_path = $material->icon->getPath();

        unset($material->icon);
        unset($material->created_at);
        unset($material->updated_at);

        $material->icon_path = $icon_path;

        return $material;

    }

    public function getResponseOk(){

        return response()->json(['status' => 1], 200);

    }

    public function getAll(){

        $result = collect(['status' => 1]);

        try{

            $materials = MaterialModel::where('is_activated', true)
                ->get();

            if($materials->count() > 0){

                foreach ($materials as &$material)
                    $material = $this->formatFields($material);

                $result = $result->merge([
                    'data' => $materials,
                    'mensagem' => 'Materiais encontrados com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Nenhum material encontrado!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar materiais!'
            ]);

            return $result;

        }

    }

    public function getMaterial(Request $request){

        if(!$request->has('material_id'))
            return ['status' => 0, 'mensagem' => 'Código do material vazio ou não encontrado.'];

        $material_id = $request->input('material_id');

        $result = collect(['status' => 1]);

        try{

            $material = MaterialModel::where([
                    ['is_activated', true],
                    ['id', $material_id],
                ])
                ->first();

            if($material){

                $material = $this->formatFields($material);

                $result = $result->merge([
                    'data' => $material,
                    'mensagem' => 'Material encontrado com sucesso.'
                ]);

            } else {

                $result = $result->merge([
                    'status' => 0,
                    'mensagem' => 'Material não encontrado!'
                ]);

            }

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar material!'
            ]);

            return $result;

        }

    }

}
