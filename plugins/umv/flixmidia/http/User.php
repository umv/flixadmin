<?php namespace UMV\Flixmidia\Http;

use Backend\Classes\Controller;
use Illuminate\Http\Request;
use UMV\Flixmidia\Models\MaterialUser;

/**
 * User Back-end Controller
 */
class User extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

    public function createMaterial(Request $request){

        if(!$request->has('material_id'))
            return ['status' => 0, 'mensagem' => 'Código do material vazio ou não encontrado.'];

        if(!$request->has('user_id'))
            return ['status' => 0, 'mensagem' => 'Código do usuário vazio ou não encontrado.'];

        if(!$request->has('data') || empty($request->input('data')))
            return ['status' => 0, 'mensagem' => 'Dados do material vazio ou não encontrado.'];

        $material_id = $request->input('material_id');
        $user_id = $request->input('user_id');
        $data = $request->input('data');

        $result = collect(['status' => 1]);

        try{

            $material_user = MaterialUser::create([
                'material_id' => $material_id,
                'user_id'   => $user_id,
                'data'  => $data
            ]);

            $result = $result->merge([
                'material_user_id' => $material_user->id,
                'mensagem' => 'Material criado com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao criar material do usuário!' . $e->getTraceAsString()
            ]);

            return $result;

        }

    }

    public function updateMaterial(Request $request){

        if(!$request->has('material_user_id'))
            return ['status' => 0, 'mensagem' => 'Código do material do usuário vazio ou não encontrado.'];

        if(!$request->has('data') || empty($request->input('data')))
            return ['status' => 0, 'mensagem' => 'Dados do material vazio ou não encontrado.'];

        $material_user_id = $request->input('material_user_id');
        $data = $request->input('data');

        $result = collect(['status' => 1]);

        try{

            MaterialUser::find($material_user_id)
                ->update([
                    'data' => $data
                ]);

            $result = $result->merge([
                'mensagem' => 'Material salvo com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao salvar material do usuário!' . $e->getTraceAsString()
            ]);

            return $result;

        }

    }

    public function deleteMaterial(Request $request){

        if(!$request->has('material_user_id'))
            return ['status' => 0, 'mensagem' => 'Código do material do usuário vazio ou não encontrado.'];

        $material_user_id = $request->input('material_user_id');

        $result = collect(['status' => 1]);

        try{

            MaterialUser::find($material_user_id)
                ->delete();

            $result = $result->merge([
                'mensagem' => 'Material removido com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao remover material do usuário!' . $e->getTraceAsString()
            ]);

            return $result;

        }

    }

    public function getMaterial(Request $request){

        if(!$request->has('material_user_id'))
            return ['status' => 0, 'mensagem' => 'Código do material do usuário vazio ou não encontrado.'];

        $material_user_id = $request->input('material_user_id');

        $result = collect(['status' => 1]);

        try{

            $material = MaterialUser::find($material_user_id);

            $result = $result->merge([
                'material' => $material,
                'mensagem' => 'Materiais encontrados com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar materiais do usuário!' . $e->getTraceAsString()
            ]);

            return $result;

        }

    }

    public function getMaterials(Request $request){

        if(!$request->has('user_id'))
            return ['status' => 0, 'mensagem' => 'Código do usuário vazio ou não encontrado.'];

        $user_id = $request->input('user_id');

        $result = collect(['status' => 1]);

        try{

            $materials = MaterialUser::where('user_id', $user_id)
                ->get();

            $result = $result->merge([
                'materials' => $materials,
                'mensagem' => 'Materiais encontrados com sucesso.'
            ]);

            return $result;

        } catch (\Exception $e) {

            $result = $result->merge([
                'status' => 0,
                'mensagem' => 'Erro ao buscar materiais do usuário!' . $e->getTraceAsString()
            ]);

            return $result;

        }

    }

}
