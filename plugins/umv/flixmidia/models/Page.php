<?php namespace UMV\Flixmidia\Models;

use Illuminate\Support\Facades\DB;
use Model;

/**
 * Model
 */
class Page extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'material' => 'UMV\Flixmidia\Models\Material'
    ];

    public $hasMany = [
        'page_object' => 'UMV\Flixmidia\Models\PageObject'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_page';

    /**
     * @param $materialID ID from material
     * @return Return the response, if query fails return false.
     */
    public function getPageByMaterial($materialID){
        // Assertion to check if we can go with query!
        assert(is_integer($materialID), 'O argumento $materialId precisa ser um inteiro.');
        //
        $response = DB::table('page')->join('material', 'material.id', '=', $materialID );
        if(is_null($response)){
            return null;
        }
        else{
            return $response;
        }

    }
}
