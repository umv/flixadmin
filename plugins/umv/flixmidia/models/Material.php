<?php namespace UMV\Flixmidia\Models;

use Doctrine\DBAL\Query\QueryException;
use Model;
use DB;

/**
 * Model
 */
class Material extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_material';

    public $attachOne = [
        'icon' => 'System\Models\File'
    ];

}
