<?php namespace UMV\Flixmidia\Models;

use Model;

/**
 * Model
 */
class PageObject extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [

    ];

    public $belongsTo = [
        'page_object_type' => 'UMV\Flixmidia\Models\PageObjectType',
        'page' => 'UMV\Flixmidia\Models\Page',
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_page_object';
}
