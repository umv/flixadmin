<?php namespace UMV\Flixmidia\Models;

use Model;

/**
 * Model
 */
class ThemePrice extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $attachOne = [
        'background' => 'System\Models\File'
    ];
    public $belongsTo = [
        'theme' => 'UMV\Flixmidia\Models\Theme'
    ];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_theme_price';

}
