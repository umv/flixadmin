<?php namespace UMV\Flixmidia\Models;

use Model;

/**
 * Model
 */
class Theme extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'cover' => 'System\Models\File'
    ];

    public $hasMany = [
        'theme_page' => 'UMV\Flixmidia\Models\ThemePage',
        'theme_price' => 'UMV\Flixmidia\Models\ThemePrice'
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_theme';

}
