<?php namespace UMV\Flixmidia\Models;

use Model;

/**
 * Model
 */
class PageObjectType extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'umv_flixmidia_page_object_type';
}
