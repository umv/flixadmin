<?php

Route::group(['prefix' => 'api/v1/flixmidia'], function () {

    Route::match(['OPTIONS', 'GET'], '/materials', 'UMV\Flixmidia\Http\Material@getAll');
    Route::match(['OPTIONS', 'GET'], '/material', 'UMV\Flixmidia\Http\Material@getMaterial');

    Route::match(['OPTIONS', 'GET'], '/pages', 'UMV\Flixmidia\Http\Page@getAll');
    Route::match(['OPTIONS', 'GET'], '/page', 'UMV\Flixmidia\Http\Page@getPage');

    Route::match(['OPTIONS', 'GET'], '/themes', 'UMV\Flixmidia\Http\Themes@getAll');
    Route::match(['OPTIONS', 'GET'], '/theme', 'UMV\Flixmidia\Http\Themes@getThemeByMaterial');

    Route::match(['OPTIONS', 'POST'], '/storage', 'UMV\Flixmidia\Http\Storage@store');

    Route::match(['OPTIONS', 'POST'], '/user/material/create', 'UMV\Flixmidia\Http\User@createMaterial');
    Route::match(['OPTIONS', 'POST'], '/user/material/update', 'UMV\Flixmidia\Http\User@updateMaterial');
    Route::match(['OPTIONS', 'POST'], '/user/material/delete', 'UMV\Flixmidia\Http\User@deleteMaterial');
    Route::match(['OPTIONS', 'GET'], '/user/materials', 'UMV\Flixmidia\Http\User@getMaterials');
    Route::match(['OPTIONS', 'GET'], '/user/material', 'UMV\Flixmidia\Http\User@getMaterial');

});