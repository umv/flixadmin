<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaMaterialUser2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_material_user', function($table)
        {
            $table->renameColumn('json_field', 'data');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_material_user', function($table)
        {
            $table->renameColumn('data', 'json_field');
        });
    }
}
