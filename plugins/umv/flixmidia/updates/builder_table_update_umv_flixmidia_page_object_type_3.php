<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaPageObjectType3 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_page_object_type', function($table)
        {
            $table->renameColumn('identificador', 'identifier');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_page_object_type', function($table)
        {
            $table->renameColumn('identifier', 'identificador');
        });
    }
}
