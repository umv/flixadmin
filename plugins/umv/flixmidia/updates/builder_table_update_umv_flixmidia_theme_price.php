<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaThemePrice extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_theme_price', function($table)
        {
            $table->renameColumn('pacth_background', 'path_background');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_theme_price', function($table)
        {
            $table->renameColumn('path_background', 'pacth_background');
        });
    }
}
