<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaPageObjectType2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_page_object_type', function($table)
        {
            $table->text('identificador');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_page_object_type', function($table)
        {
            $table->dropColumn('identificador');
        });
    }
}
