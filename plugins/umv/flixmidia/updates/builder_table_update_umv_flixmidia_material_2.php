<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaMaterial2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_material', function($table)
        {
            $table->integer('page_id');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_material', function($table)
        {
            $table->dropColumn('page_id');
        });
    }
}
