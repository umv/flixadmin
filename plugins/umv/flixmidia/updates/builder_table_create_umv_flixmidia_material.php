<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaMaterial extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_material', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('path_icon');
            $table->boolean('is_activated');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_material');
    }
}
