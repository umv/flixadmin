<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaMaterialUser extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_material_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('material_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_material_user');
    }
}
