<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaTheme extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_theme', function($table)
        {
            $table->integer('theme_page_id');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_theme', function($table)
        {
            $table->dropColumn('theme_page_id');
        });
    }
}
