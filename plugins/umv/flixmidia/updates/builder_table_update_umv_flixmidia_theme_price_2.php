<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaThemePrice2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_theme_price', function($table)
        {
            $table->dropColumn('path_background');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_theme_price', function($table)
        {
            $table->string('path_background', 100)->nullable();
        });
    }
}
