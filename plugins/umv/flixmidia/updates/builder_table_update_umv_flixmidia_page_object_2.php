<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaPageObject2 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_page_object', function($table)
        {
            $table->integer('page_id')->nullable()->change();
            $table->integer('page_object_type_id')->nullable()->change();
            $table->integer('index')->nullable()->change();
            $table->integer('width')->nullable()->change();
            $table->integer('height')->nullable()->change();
            $table->integer('position_y')->nullable()->change();
            $table->integer('position_x')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_page_object', function($table)
        {
            $table->integer('page_id')->nullable(false)->change();
            $table->integer('page_object_type_id')->nullable(false)->change();
            $table->integer('index')->nullable(false)->change();
            $table->integer('width')->nullable(false)->change();
            $table->integer('height')->nullable(false)->change();
            $table->integer('position_y')->nullable(false)->change();
            $table->integer('position_x')->nullable(false)->change();
        });
    }
}
