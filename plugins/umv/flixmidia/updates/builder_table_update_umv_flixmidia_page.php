<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaPage extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_page', function($table)
        {
            $table->text('name');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_page', function($table)
        {
            $table->dropColumn('name');
        });
    }
}
