<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaPageObject extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_page_object', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('page_id');
            $table->integer('page_object_type_id');
            $table->integer('index');
            $table->integer('y');
            $table->integer('x');
            $table->integer('width');
            $table->integer('height');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_page_object');
    }
}
