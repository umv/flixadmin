<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaPage extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_page', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('material_id');
            $table->integer('width');
            $table->integer('height');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_page');
    }
}
