<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaThemePage extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_theme_page', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('theme_id');
            $table->integer('page_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_theme_page');
    }
}
