<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaThemePrice extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_theme_price', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('theme_id');
            $table->text('color_text');
            $table->text('color_background');
            $table->text('pacth_background');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_theme_price');
    }
}
