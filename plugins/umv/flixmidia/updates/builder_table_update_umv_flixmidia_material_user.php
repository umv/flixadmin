<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaMaterialUser extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_material_user', function($table)
        {
            $table->text('json_field');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_material_user', function($table)
        {
            $table->dropColumn('json_field');
        });
    }
}
