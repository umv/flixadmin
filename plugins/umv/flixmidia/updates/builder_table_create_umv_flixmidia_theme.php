<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaTheme extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_theme', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->dateTime('started_at');
            $table->dateTime('finalized_at');
            $table->text('path_cover');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_theme');
    }
}
