<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaMaterial4 extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_material', function($table)
        {
            $table->text('identifier');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_material', function($table)
        {
            $table->dropColumn('identifier');
        });
    }
}
