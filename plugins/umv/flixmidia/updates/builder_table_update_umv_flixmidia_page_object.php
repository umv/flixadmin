<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateUmvFlixmidiaPageObject extends Migration
{
    public function up()
    {
        Schema::table('umv_flixmidia_page_object', function($table)
        {
            $table->integer('position_y');
            $table->integer('position_x');
            $table->dropColumn('y');
            $table->dropColumn('x');
        });
    }
    
    public function down()
    {
        Schema::table('umv_flixmidia_page_object', function($table)
        {
            $table->dropColumn('position_y');
            $table->dropColumn('position_x');
            $table->integer('y');
            $table->integer('x');
        });
    }
}
