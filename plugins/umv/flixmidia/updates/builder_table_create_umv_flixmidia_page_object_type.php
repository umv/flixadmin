<?php namespace UMV\Flixmidia\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateUmvFlixmidiaPageObjectType extends Migration
{
    public function up()
    {
        Schema::create('umv_flixmidia_page_object_type', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('umv_flixmidia_page_object_type');
    }
}
