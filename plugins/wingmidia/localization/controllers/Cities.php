<?php namespace WingMidia\Localization\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Cities extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'wingmidia_localization_cities' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('WingMidia.Localization', 'main-menu-item', 'side-menu-item');
    }
}
