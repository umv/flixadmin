<?php namespace WingMidia\Localization\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration104 extends Migration
{
    public function up()
    {
        Schema::table('wingmidia_localization_cities', function ($table) {
            
            $table->dropColumn('state_id');
            
        });
    }

    public function down()
    {
        Schema::table('wingmidia_localization_cities', function ($table) {
            $table->integer('state_id');
        });
    }
}