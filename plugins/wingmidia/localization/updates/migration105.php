<?php namespace WingMidia\Localization\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class Migration105 extends Migration
{
    public function up()
    {
        Schema::table('wingmidia_localization_cities', function ($table) {
            
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('wingmidia_localization_states');
            
        });
    }

    public function down()
    {
        Schema::table('wingmidia_localization_cities', function ($table) {
            
            $table->dropForeign('wingmidia_localization_cities_state_id_foreign');
            
        });
    }
}