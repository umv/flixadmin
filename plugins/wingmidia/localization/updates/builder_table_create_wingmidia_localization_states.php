<?php namespace WingMidia\Localization\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateWingmidiaLocalizationStates extends Migration
{
    public function up()
    {
        Schema::create('wingmidia_localization_states', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->string('uf', 2);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('wingmidia_localization_states');
    }
}
