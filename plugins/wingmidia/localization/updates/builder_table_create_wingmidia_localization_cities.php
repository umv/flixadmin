<?php namespace WingMidia\Localization\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateWingmidiaLocalizationCities extends Migration
{
    public function up()
    {
        Schema::create('wingmidia_localization_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('state_id');
            $table->text('name');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('wingmidia_localization_cities');
    }
}
